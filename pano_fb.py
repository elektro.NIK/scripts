#!/usr/bin/env python3
import subprocess
from sys import argv
from PIL import Image

# input data
width_degree = int(argv[1])
filename = argv[2]

# check input
if width_degree < 90 or width_degree > 360:
    print("ERROR! Width degree value wrong (must be 90...360)")
    exit(1)

# image data
im = Image.open(filename)
width, height = im.size

# check image data
if height > width:
    print("ERROR! Width less than height.")
    exit(1)
if height == width:
    print("ERROR! Width = height")
    exit(1)
if width >= 30000 or height >= 30000:
    print("ERROR! Very large file (over 30000 p by side)")
    exit(1)
if height * width >= 135000000:
    print(f"ERROR! Very large file: {height * width / 1000000} Mp (must be less than 135 Mp)")
    exit(1)

# calculating other input data
height_degree = int(width_degree * height / width)

# check other data
if height_degree > 180:
    print(f"ERROR! Height degree value wrong: {height_degree} (must be 0...180)")
    exit(1)

# output data
projection = "equirectangular" if width_degree == 360 and height_degree == 180 else "cylindrical"
full_width = int(width * 360 / width_degree)
full_height = int(full_width / 2)
offset_left = int((full_width - width) / 2)
offset_up = int((full_height - height) / 2)

command = f"exiftool \
-ProjectionType={projection} \
-UsePanoramaViewer=true \
-FullPanoWidthPixels={full_width} \
-FullPanoHeightPixels={full_height} \
-CroppedAreaLeftPixels={offset_left} \
-CroppedAreaTopPixels={offset_up} \
-CroppedAreaImageWidthPixels={width} \
-CroppedAreaImageHeightPixels={height}"

process = subprocess.Popen(command.split(' ') + [filename], stdout=subprocess.PIPE)
output, error = process.communicate()

print(output.decode()) if output else None
print(error.decode()) if error else None
